// This material is ©, 2022, John Krug (jh.krug@gmail.com).
[subs="attributes"]
[#{yt_anchor}]
.{yt_title}
****
ifdef::backend-pdf[]
https://youtu.be/{yt_video_id}[]
endif::[]
ifndef::backend-pdf[]
[subs="attributes"]
++++
<div style="padding-top:2%">
  <div style="position:relative;padding-top:56.25%;">
    <iframe allowfullscreen frameborder="0"
      style="position:absolute;top:0;left:0;width:100%;height:100%;"
      src="https://www.youtube.com/embed/{yt_video_id}?controls=1&modestbranding=1&rel=0">
    </iframe>
  </div>
</div>
++++
endif::[]
****