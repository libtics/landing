// This material is ©, 2022, John Krug (jh.krug@gmail.com).
include::chapter_top_inc.adoc[]

= libtics.org

== What is it?

The project, https://libtics.org[libtics.org], began as an enquiry from the
University of Leeds, via Ex Libris, about providing some training materials for
internal use. Leeds were interested in developing analytics capability within
the institution, having recently (in 2019) become users of Alma.

The project started in early 2020, just as the pandemic was picking
up momentum. The current work has now mostly finished. Leeds wanted a
comprehensive introduction to working with Alma Analytics. In agreement with
them, I developed a set of materials.

I always had in mind that perhaps this material may be reusable and useful more
widely. I would like the material to be open source. From the start a set of
tools was chosen that would make it easier to generate multiple output formats
including the libtics.org website. The material is written in asciidoc markup
and outputs are generated using asciidoctor and antora.

So, libtics is a resource for library professionals using Ex Libris products
and who need to start getting to grips with Analytics at their institutions.
Possible areas of extension might be into other library systems analytics
offering, library analytics in general, more general stuff on the process of
developing analytics, best practices, statistics, etc.

I hope that libtics can be a platform for the future development and
maintenance of this type of training content.

The initial set of material at xref:aapl:ROOT:frontispiece.adoc[Ex Libris
Analytics] covers:

* Introduction
* Using analytics in practice
* Understanding your data
* In more depth
* Dashboards
* Reusing components
* Format, style, charting
* Primo analytics
* Oracle Data Visualization -- the new analytics component since October 2020.

Next up will be:

* Some Leganto
* Extraction/Export/APIs
* More examples, especially to do with e-resources and workflow
* In greater depth on some topics.

== What it isn't.

This is not an alternative to IGeLUs Analytics community of practice
(previously the SIWG). The first resource here at libtic.org provides Ex Libris
analytics training, perhaps complimentary to that mission.

== Why is libtics.org different to other resources?

There are plenty of available resources on the InterWebs, notably by larger
institutions around the time they were implementing Alma. Why try to do it this
way?

* Community based open project, not by a single institution -- Built by
the users of Ex Libris products to satisfy their and their institutions
requirements for analytics training materials.

* Developed like code -- training materials developed as code. Meaning that all
you need is a text editor, the development environment, and maybe something
to record video, to develop content for libtics.org. Then your material, the
code, is version controlled, committed to the repository with a commit message.
A set of continuous integration processing pipelines can be activated locally
for development, or remotely for deployment. Content is deployed on a leading
static content delivery network, https://netlify.com[netlify.com]. Algolia
search is integrated using a Netlify add-on.

* Tooling -- a text editor of your choice, asciidoc, asciidoctor, antora, git
version control, GitLab, are all freely available. Code is written in the
asciidoc markup format and stored in plain text files. This can be used to
generate the entire libtics.org website (or even book ready PDF) by processing
with asciidoctor and antora. No proprietary tools needed. Though, I have been
using https://www.screencastify.com[Screencastify] to record and edit
screencast video.

* GitLab -- The code repository is GitLab with built in tools for project
management and issue tracking and the ability to create manage and merge
different functional branches of development.

* Maintainability -- Training materials deteriorate over time. Alma, Primo
and their analytics packages change over time so that the functionality, and
it's description in the training materials drift apart. If libtics is
maintainable then a good, correct match between the materials and the services
it is describing is easier to achieve. The tool chain and GitLab help provide
for that maintainability. Of course, a good community of people will still
be needed to perform the actual maintenance and development using those
facilities.

* Zulip -- chat at https://libtics.zulipchat.com. Very nice chat/forum tool.
